using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace RacingView
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        GraphicsDevice device;
        

        BoardVisual board;
        ButtonVisual button1;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            Window.AllowUserResizing = true;
            graphics.PreferMultiSampling = true;
        }


        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            device = graphics.GraphicsDevice;

            board = new BoardVisual(GraphicsDevice);
            button1 = new ButtonVisual(GraphicsDevice, "text", 40, 40, 10, 3);

            //board = Content.Load<Texture2D>("Dostihy-a-sazky-Hraci-plan");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            MouseState st = Mouse.GetState();



            if (st.LeftButton == ButtonState.Pressed)
            {
                if (button1.isPointOver(st.X, st.Y))
                {
                    button1.Handled = true;

                }

                
            }

            if (st.LeftButton == ButtonState.Released)
            {
                if (button1.isPointOver(st.X, st.Y))
                {
                    if (button1.Handled)
                    {
                        //udelam akci
                        Console.WriteLine("akce");
                    }

                    button1.Handled = false;

                }
                else
                    button1.Handled = false;


            }



            base.Update(gameTime);



        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();

            //spriteBatch.Draw(board, new Vector2(0, 0), Color.White);
            board.Draw(spriteBatch, device.PresentationParameters.BackBufferHeight, device.PresentationParameters.BackBufferHeight);
            button1.Draw(spriteBatch, device.PresentationParameters.BackBufferHeight, device.PresentationParameters.BackBufferHeight);
            spriteBatch.End();

            
            

            base.Draw(gameTime);
        }
    }
}
