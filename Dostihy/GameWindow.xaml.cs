﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Racing;

namespace Dostihy
{
	/// <summary>
	/// Interaction logic for GameWindow.xaml
	/// </summary>
	public partial class GameWindow : Window
	{
		internal Racing.Game Game;
		 
		public GameWindow()
		{
			this.InitializeComponent();
			
			// Insert code required on object creation below this point.

			Game = new Racing.Game();

			Game.AddPlayer("Tomas");
			Game.AddPlayer("Honza");
			Game.AddPlayer("Josef");
			Game.GameStart();

			playersListBox.ItemsSource = Game.Players;
			fieldsListBox.ItemsSource = Game.Board.Fields;
			playersListBox.SelectedItem = Game.CurrentPlayer;
			finishTurn.IsEnabled = Game.FinishTurnEnabled;
		}

		private void finishTurn_Click(object sender, RoutedEventArgs e)
		{
			Game.FinishTurn();
			playersListBox.SelectedItem = Game.CurrentPlayer;
			betButton.IsEnabled = Game.BetsEnabled;
			rollButton.IsEnabled = Game.RollDiceEnabled;
			finishTurn.IsEnabled = Game.FinishTurnEnabled;
		}

		private void buyButton_Click(object sender, RoutedEventArgs e)
		{
			Game.CurrentPlayer.Buy();
		}

		private void rollButton_Click(object sender, RoutedEventArgs e)
		{
			Game.RollDiceAndMove();
			fieldsListBox.SelectedIndex = Game.CurrentPlayer.CurrentPosition.Location;
			betButton.IsEnabled = Game.BetsEnabled;
			rollButton.IsEnabled = Game.RollDiceEnabled;
			finishTurn.IsEnabled = Game.FinishTurnEnabled;
		}
	}
}