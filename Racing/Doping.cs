﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Racing
{
    class Doping : Field
    {
        /// <summary>
        /// Karta finance
        /// </summary>
        /// <param name="location"></param>
        /// <param name="safeName"></param>
        public Doping(int location, string safeName)
            : base(location, safeName)
        {
            TypeOfField = FieldType.Doping;
        }

        /// <summary>
        /// Hráč vstoupil na políčko, hráč jedno kolo nehraje
        /// </summary>
        /// <param name="player">Hráč, který vstoupil na políčko</param>
        /// <param name="rolledValue">Hodnota hozená na kostce</param>
        public override void Enter(Player player, int rolledValue)
        {
            base.Enter(player, rolledValue);
            Debug.WriteLine("provede podezření z dopingu");
            player.DopingSuspicion(1);
        }

    }
}
