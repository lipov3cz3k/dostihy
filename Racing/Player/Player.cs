﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Racing
{
    public class Player
    {
        private string name;
        private bool isAlive;
        private int budget;
        private bool isInDistance = false;
        private bool dopingDelay = false;
        private int delayCount = 0;
        private Field currentPosition;

        /// <summary>
        /// Konstruktor hráče
        /// </summary>
        /// <param name="startField">Políčko, kde hráč začíná</param>
        /// <param name="name">Jméno hráče</param>
        /// <param name="startBudget">Počáteční rozpočet</param>
        public Player(Field startField, string name, int startBudget)
        {
            Name = name;
            Budget = startBudget;
            IsAlive = true;
            currentPosition = startField;
        }

        /// <summary>
        /// Hráč se přesune na jinou pozici, pokud projel startem, tak obdrží částku incomeOnStart
        /// </summary>
        /// <param name="newPosition">Nová pozice hráče</param>
        public void Move(Field newPosition){
            if (newPosition == null)
                throw new ApplicationException("Undefined position!");
            this.currentPosition = newPosition;
        }

        /// <summary>
        /// Seznam akcí, které může hráč provést na aktuálním políčku
        /// </summary>
        public List<Field.Action> GetAvailableActions
        {
            get
            {
                return this.CurrentPosition.AvailableActions(this);
            }
        }

        /// <summary>
        /// Hráč nakoupí zadaný počet žetonů na aktuálního koně (aktuální pozice)
        /// </summary>
        /// <param name="count">Počet kupovaných žetonů</param>
        public void BuyToken(int count)
        {
            // TODO zbytečná paranoia
            if ((this.GetAvailableActions.Contains(Field.Action.CanBuyOneToken) && count == 1) ||
            (this.GetAvailableActions.Contains(Field.Action.CanBuyTwoTokens) && count == 2) ||
            (this.GetAvailableActions.Contains(Field.Action.CanBuyThreeTokens) && count == 3) ||
            (this.GetAvailableActions.Contains(Field.Action.CanBuyFourTokens) && count == 4) ||
            (this.GetAvailableActions.Contains(Field.Action.CanBuyMainToken) && count == 1))
            {
                ((Horse)this.CurrentPosition).NTokens += count;
                this.Budget -= count * ((Horse)this.CurrentPosition).TokenPrice;
            }
            else
                throw new ApplicationException("Cannot buy specified number of tokens."); // TODO dokumentovat vyjímku
        }

        /// <summary>
        /// Hráči se odečte zadaná částka TODO - pokud hráč nemá peníze
        /// </summary>
        /// <param name="amount">Placená částka</param>
        public void PostPaymant(int amount)
        {
            if (this.Budget >= amount)
            {
                // platící má ještě dost peněz
                this.Budget -= amount;
            }
            else
            {
                //TODO hráč musí něco prodat, jinak bankrot hráče !!
                Debug.WriteLine("Hráč nemá k dispozici peníze - musí něco prodat. TODO");
            }

            Debug.WriteLine(" << "+ this.Name + " zaplatil " + amount);
        }

        /// <summary>
        /// Hráč příjme zadanou částku
        /// </summary>
        /// <param name="amount">Příchozí platba</param>
        public void ReceivePaymant(int amount)
        {
            this.Budget += amount;
            Debug.WriteLine(" >> " + this.Name + " přijal " + amount);
        }

        /// <summary>
        /// Hráč koupí políčko na kterém stojí
        /// </summary>
        public void Buy()
        {
            if (this.GetAvailableActions.Contains(Field.Action.CanBuy))
                ((Buyable)this.CurrentPosition).Buy(this);
            else
                //TODO Dopsat do dokumentace vyjímku
                throw new ApplicationException("Player cannot buy this item - action is not available.");
        }

        /// <summary>
        /// Vrací true, pokud hráč může vybírat poplatky z dostihů (není v distance, nebo v dopingu)
        /// </summary>
        /// <returns>True, pokud hráč není v distance, nebo v podezření z dopingu</returns>
        public bool TokensIsActive()
        {
            if (IsInDistance && this.dopingDelay)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Hráč je podezřelý z dopingu, nehraje zadaný počet kol
        /// </summary>
        /// <param name="delayCount">Počet kol, které hráč nehraje</param>
        public void DopingSuspicion(int delayCount)
        {
            dopingDelay = true;
            Delay = delayCount;
        }

        /// <summary>
        /// Vrací true, pokud není hráč zdržen, jinak false a zároveň se odečte jedno kolo ze zdržení
        /// </summary>
        /// <returns>True, pokud není hráč zdržen</returns>
        public bool CanPlay()
        {
            if (delayCount <= 0)
                return true;
            else
                return false;
        }

        //TODO Refactoring - predelat na volani pred zacatkem kola - kvuli delay counteru
        public void Finish()
        {
            if (delayCount > 0)
                delayCount--;
            if(delayCount == 0)
                dopingDelay = false;
        }

        public override string ToString()
        {
            return Name;
        }

        #region Properties - getters and setters
        /// <summary>
        /// Gets or sets player's name
        /// </summary>
        public string Name
        {
            get { return name; }
            private set { name = value; }
        }

        /// <summary>
        /// Gets or sets if is player alive.
        /// </summary>
        public bool IsAlive
        {
            get { return isAlive; }
            internal set { isAlive = value; }
        }

        /// <summary>
        /// Gets actual player's position.
        /// </summary>
        public Field CurrentPosition
        {
            get { return currentPosition; }
        }

        /// <summary>
        /// Gets or sets player's budget.
        /// </summary>
        public int Budget
        {
            get { return budget; }
            private set { budget = value; }
        }

        /// <summary>
        /// Gets or sets true, if player is in the distance field
        /// </summary>
        public bool IsInDistance
        {
            get { return isInDistance; }
            set { isInDistance = value; }
        }

        /// <summary>
        /// Gets or sets value, how many rounds player can not play.
        /// Setter adds one more round, becouse of current round
        /// </summary>
        public int Delay
        {
            get { return this.delayCount; }
            set { this.delayCount = value + 1; }
        }

        #endregion
    }
}
