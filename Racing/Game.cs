﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Racing.Properties;
using System.Collections.Generic;

namespace Racing
{
    public class Game
    {
        private int GAME_MAX_PLAYERS = 10;

        private Board board;
        private Players players = new Players();

        private int startBudget;
        private int incomeOnStart;
        private Random random = new Random();
        private int rolledValue = 0;


        public Game()
        {
            StartBudget = 30000; // init value
            IncomeOnStart = 4000; // init value

            this.GameInit();
        }

        /// <summary>
        /// Initializes game (prepare game board)
        /// </summary>
        private void GameInit()
        {
            Settings.Default.Reload();
            try
            {
                this.board = new Board();
            }
            catch (FileNotFoundException exc)
            {
                Debug.WriteLine(exc.Message + "Soubor s rozložením desky není k dispozici.");
            }
        }

        /// <summary>
        /// Starts the game, actual player is ready.
        /// </summary>
        public void GameStart()
        {
            this.NextPlayer();
            this.RollDiceEnabled = true;
            this.FinishTurnEnabled = false;
        }

        /// <summary>
        /// Switches for next player. Actual player is finished.
        /// </summary>
        private void NextPlayer()
        {
            CurrentPlayer.Finish();
            players.Next();

            if (!CurrentPlayer.CanPlay())
            {
                Debug.WriteLine("Hrac toto kolo nehraje - prepinam na dalsiho");
                NextPlayer();
            }
        }


        /// <summary>
        /// Roll the dice and move player to the new position. Checks distance conditions (go in and out of distance).
        /// </summary>
        public void RollDiceAndMove()
        {
            if (!this.RollDiceEnabled){
                Debug.WriteLine("Hod kostkou neni povolen!");
                throw new ApplicationException("Dice roll is not allowed!");
            }

            rolledValue = 0;
            this.RollDice();
            if (!CurrentPlayer.IsInDistance || (CurrentPlayer.IsInDistance && CheckOutOfDistance()))
            {
                this.CheckSixes();
                this.MoveForRolledValue();
                this.PostRoll();
            }
            else
            {
                // stay in distance - TODO can use magic card!
                Debug.WriteLine("Hráč zůstává v distance");
                this.PostRoll();
            }

            foreach (var item in CurrentPlayer.GetAvailableActions)
            {
                Debug.WriteLine("možnosti: " + item.ToString());
            }
        }
        
        /// <summary>
        /// Simulates dice roll - generates random value and adds it to previous rolled value
        /// </summary>
        private void RollDice()
        {
            rolledValue += random.Next(1, 7);
        }

        /// <summary>
        /// Returns true if the dice value means leave distance field, if so then new dice value is prepared
        /// </summary>
        /// <returns></returns>
        private bool CheckOutOfDistance()
        {
            if (rolledValue == 6)
            {
                Debug.WriteLine("Na kostce padla hodnota 6, hrac vystupuje z distance.");
                rolledValue = 0;
                RollDice();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// If previous dice value was 6, then new dice roll is initialized
        /// </summary>
        private void CheckSixes()
        {
            if(rolledValue == 6) {
                Debug.WriteLine("Na kostce padla hodnota 6, házím znovu");
                RollDice();
            }
        }

        /// <summary>
        /// Move player for dice value. If dice value is 12, then player goes to distance.
        /// </summary>
        private void MoveForRolledValue()
        {
            if (rolledValue == 12)
            {
                Debug.WriteLine("Hozeno 2x 6, hráč jde do distance.");
                this.GoToDistance();
            }
            else
            {
                int newPosition = (CurrentPlayer.CurrentPosition.Location + rolledValue) % (board.Fields.Last.Value.Location + 1);
                MoveToNewPosition(newPosition);
            }
        }

        private bool MoveToNewPosition(int newPosition, bool regularMove = true)
        {
            bool passStart = false;  
            if (newPosition - CurrentPlayer.CurrentPosition.Location <= 0)
            {
                // prosel startem
                passStart = true;
                if (regularMove)
                {
                    CurrentPlayer.ReceivePaymant(this.IncomeOnStart);
                }
            }

            CurrentPlayer.Move(this.board.Fields.ElementAt(newPosition));
            CurrentPlayer.CurrentPosition.Enter(CurrentPlayer, rolledValue); // TODO vymyslet lepší volání že hráč vstoupil na políčko

            return passStart;
        }

        private void GoToDistance()
        {
            //int x = (Board.Fields.Count - current + distance) % Board.Fields.Count; // o kolik se hrac posune
            int distance = Board.Fields.First(field => field.TypeOfField == Field.FieldType.Distance).Location;
            MoveToNewPosition(distance, false);
        }

        /// <summary>
        /// Sets post roll atributes (disable another rolling and betting)
        /// </summary>
        private void PostRoll()
        {
            this.RollDiceEnabled = false;
            this.BetsEnabled = false;
            this.FinishTurnEnabled = true;
        }

        /// <summary>
        /// Adds player to the list of players. Checks if there is not too many players.
        /// </summary>
        /// <param name="player">New player</param>
        public void AddPlayer(Player player)
        {
            if (this.players.Count() < GAME_MAX_PLAYERS)
            {
                players.AddLast(player);
            }
            else
            {
                throw new ApplicationException("Too many players!");
            }
        }

        /// <summary>
        /// Creates new player at start position, adds player to the list of players. Checks if there is not too many players.
        /// TODO Throw exeption if no board  Před přidáním hráče je třeba nejdřív zavolat metodu PrepareGame() pro inicializaci herní desky.
        /// </summary>
        /// <param name="name">New player's name</param>
        public void AddPlayer(string name)
        {
            this.AddPlayer(new Player(this.Board.Fields.ElementAt(0), name, this.StartBudget));
        }

        /// <summary>
        /// Finish current player's turn
        /// </summary>
        public void FinishTurn()
        {
            if (!this.RollDiceEnabled) // TODO co když jedno kolo nehraje?
            {
                this.NextPlayer();
                this.RollDiceEnabled = true;
                this.BetsEnabled = true;
                this.FinishTurnEnabled = false;
            }
        }


        #region Properties

        /// <summary>
        /// Gets players list
        /// </summary>
        public LinkedList<Player> Players
        {
            get { return this.players.PlayersList; }
        }

        /// <summary>
        /// Gets current player
        /// </summary>
        public Player CurrentPlayer
        {
            get { return this.players.CurrentPlayer; }
        }

        /// <summary>
        /// Gets board
        /// </summary>
        public Board Board
        {
            get { return board; }
        }

        /// <summary>
        /// Gets or sets contribution for start field
        /// </summary>
        internal int IncomeOnStart
        {
            get { return incomeOnStart; }
            set { incomeOnStart = value; }
        }

        /// <summary>
        /// Gets or sets start budget of player
        /// </summary>
        internal int StartBudget
        {
            get { return startBudget; }
            set { startBudget = value; }
        }

        /// <summary>
        /// Gets or sets state, if is the bets enabled
        /// </summary>
        public bool BetsEnabled
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets or sets state, if are dice rolling enables
        /// </summary>
        public bool RollDiceEnabled
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets or sets sate, if turn can be finished
        /// </summary>
        public bool FinishTurnEnabled
        {
            get;
            internal set;
        }

        #endregion


    }
}
