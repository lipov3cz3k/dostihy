﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Racing;

namespace Racing
{
    class Players
    {
        private LinkedList<Player> players;
        private LinkedListNode<Player> currentPlayer;

        /// <summary>
        /// Seznam hráčů v kruhu
        /// </summary>
        public Players()
        {
            this.players = new LinkedList<Player>();
        }

        /// <summary>
        /// Počet hráčů v seznamu
        /// </summary>
        /// <returns>Počet hráčů v seznamu</returns>
        internal int Count()
        {
            return this.players.Count();
        }

        /// <summary>
        /// Přidá na konec seznamu nového hráče a zároveň ho nastaví jako aktuálního
        /// </summary>
        /// <param name="player">Přidávaný hráč</param>
        internal void AddLast(Player player)
        {
            this.players.AddLast(player);
            this.currentPlayer = this.players.Last;
        }

        /// <summary>
        /// Nastaví dalšího hráče v seznamu (na konci seznamu pokračuje od začátku) jako aktuálního
        /// </summary>
        /// <returns>Nový aktuální hráč</returns>
        internal Player Next()
        {
            if (this.players.Count == 1) // hráč je sám            
                throw new ApplicationException("Poslední hráč, měla by zkončit hra");

            if (currentPlayer.Next == null) // hráč je poslední
                currentPlayer = currentPlayer.List.First;
            else
                currentPlayer = currentPlayer.Next;
            return CurrentPlayer;
        }

        /// <summary>
        /// Odebere aktuálního hráče ze seznamu
        /// </summary>
        /// <returns>Vrací true</returns>
        internal void Remove()
        {
            players.Remove(currentPlayer);
        }

        /// <summary>
        /// Odebere zadaného hráče ze seznamu
        /// </summary>
        /// <param name="player">Odebíraný hráč</param>
        /// <returns>Vrací true, pokud se podařilo hráče odebrat, jinak false.
        /// Vrací false pokud nebyl hráč nalezen.</returns>
        internal bool Remove(Player player)
        {
            return players.Remove(player);
        }

        /// <summary>
        /// Aktuální hráč (na řadě)
        /// </summary>
        public Player CurrentPlayer
        {
            get { return currentPlayer.Value; }
        }

        public LinkedList<Player> PlayersList
        {
            get { return players; }
        }
    }
}
