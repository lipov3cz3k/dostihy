﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Diagnostics;
using Racing;
using Racing.Properties;

namespace Racing
{
    
    public class Board
    {
        private LinkedList<Field> fields;

        public LinkedList<Field> Fields
        {
            get { return fields; }
        }

        private List<Stud> studs;
        private CardStack chanceCards;
        private CardStack financeCards;

        public Board()
        {
            // vytvori herni policka
            fields = new LinkedList<Field>();
            studs = new List<Stud>();

            Random random = new Random();
            chanceCards = new CardStack(14, random);
            financeCards = new CardStack(14, random);

            XElement xmlFile = XElement.Load(Settings.Default.fieldsDataFile);
            LoadFields(xmlFile);
            LoadStuds(xmlFile);
            ConnectTrainers();
            ConnectUtilities();
        }

        private void LoadFields(XElement xmlFile)
        {
            foreach (XElement field in xmlFile.Element("fields").Elements("field"))
            {
                switch (field.Attribute("type").Value)
                {
                    case "SAFE":
                        fields.AddLast(new Field(System.Convert.ToInt32(field.Attribute("location").Value), field.Attribute("safename").Value));
                        break;
                    case "HORSE":
                        List<int> charges = new List<int>(6);
                        foreach (XElement charge in field.Elements("prices").Elements("charges").Elements("charge"))
                        {
                            charges.Add(System.Convert.ToInt32(charge.Value));
                        }
                        fields.AddLast(new Horse(System.Convert.ToInt32(field.Attribute("location").Value),
                                                System.Convert.ToInt32(field.Element("prices").Element("base").Value),
                                                System.Convert.ToInt32(field.Element("prices").Element("token").Value),
                                                charges,
                                                field.Attribute("safename").Value
                                                ));
                        break;
                    case "TRAINER":
                        fields.AddLast(new Trainer(System.Convert.ToInt32(field.Attribute("location").Value),
                                                    System.Convert.ToInt32(field.Element("prices").Element("base").Value),
                                                    System.Convert.ToInt32(field.Element("prices").Element("charge").Value), 
                                                    field.Attribute("safename").Value
                                                    ));
                        break;
                    case "UTILITY":
                        List<int> taxFactor = new List<int>(2);
                        foreach (XElement tax in field.Elements("prices").Elements("taxes").Elements("tax"))
                        {
                            taxFactor.Add(System.Convert.ToInt32(tax.Value));
                        }
                        fields.AddLast(new Utility(System.Convert.ToInt32(field.Attribute("location").Value),
                                                    System.Convert.ToInt32(field.Element("prices").Element("base").Value),
                                                    taxFactor, 
                                                    field.Attribute("safename").Value
                                                    ));
                        break;
                    case "CHANCE":
                        fields.AddLast(new Chance(System.Convert.ToInt32(field.Attribute("location").Value), 
                                                    field.Attribute("safename").Value, 
                                                    chanceCards
                                                    ));
                        break;
                    case "FINANCE":
                        fields.AddLast(new Finance(System.Convert.ToInt32(field.Attribute("location").Value), 
                                                    field.Attribute("safename").Value,
                                                    financeCards
                                                    ));
                        break;
                    case "VETERINARY":
                        fields.AddLast(new Veterinary(System.Convert.ToInt32(field.Attribute("location").Value), 
                                                    field.Attribute("safename").Value,
                                                    System.Convert.ToInt32(field.Attribute("charge").Value)
                                                    ));
                        break;
                    case "DISTANCE":
                        fields.AddLast(new Distance(System.Convert.ToInt32(field.Attribute("location").Value), 
                                                    field.Attribute("safename").Value
                                                    ));
                        break;
                    case "DOPING":
                        fields.AddLast(new Doping(System.Convert.ToInt32(field.Attribute("location").Value),
                                                    field.Attribute("safename").Value
                                                    ));
                        break;
                    default:
                        Debug.WriteLine("unknown field type");
                        break;
                }
            }
        }

        private void LoadStuds(XElement xmlFile)
        {
            int id = 0;
            foreach (XElement stud in xmlFile.Element("studs").Elements("stud"))
            {
                List<Horse> horses = new List<Horse>();
                Horse horse;
                foreach (XElement location in stud.Elements("location"))
                {
                    if (fields.ElementAt(System.Convert.ToInt32(location.Value)).TypeOfField != Field.FieldType.Horse)
                    {
                        throw new ApplicationException("Error in field type conversion. Field is not a horse.");
                    }
                    horse = (Horse)fields.ElementAt(System.Convert.ToInt32(location.Value));
                    horses.Add(horse);
                }
                studs.Add(new Stud(id,ref horses));
                id++;
            }
        }

        /// <summary>
        /// Přidá do všech objektů Trainer.otherTrainers odkazy na ostatní trenéry
        /// </summary>
        private void ConnectTrainers()
        {
            List<Trainer> allTrainers = fields.Where(trainer => trainer.TypeOfField == Field.FieldType.Trainer).Select(trainer => (Trainer)trainer).ToList();
            foreach (Trainer item in allTrainers)
            {
                item.otherTrainers = allTrainers.Where(trainer => trainer != item).ToList();
            }
        }

        /// <summary>
        /// Přidá do všech objektů Utility.otherUtilities odkazy na ostatní utility (stáje, přeprava)
        /// </summary>
        private void ConnectUtilities()
        {
            List<Utility> allUtilities = fields.Where(utility => utility.TypeOfField == Field.FieldType.Utility).Select(utility => (Utility)utility).ToList();
            foreach (Utility item in allUtilities)
            {
                item.otherUtilities = allUtilities.Where(utility => utility != item).ToList();
            }
        }
    }
}
