﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Racing
{
    internal class CardStack
    {
        private List<int> shuffled;

        public CardStack(int nCards, Random random)
        {
            if (nCards <= 0)
                throw new ApplicationException("Zero stack count.");
            
            //shuffled = new List<int>();

            Random rnd = new Random();

            shuffled = Enumerable.Range(0, nCards).ToList();
            Shuffle(random);
        }

        private void Shuffle(Random random)
        {
            int n = this.shuffled.Count;

            while (n > 1)
            {
                n--;
                int k = random.Next(n + 1);
                int tmp = shuffled[k];
                shuffled[k] = shuffled[n];
                shuffled[n] = tmp;
            }
        }
            
    }
}
