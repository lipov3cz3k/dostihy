﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Racing
{
    internal class Finance : Field
    {

        private CardStack cardStack;

        /// <summary>
        /// Karta finance
        /// </summary>
        /// <param name="location"></param>
        /// <param name="safeName"></param>
        public Finance(int location, string safeName, CardStack cardStack)
            : base(location, safeName)
        {
            TypeOfField = FieldType.Finance;
            this.cardStack = cardStack;
        }

        /// <summary>
        /// Hráč vstoupil na políčko, provede se ukol finance
        /// </summary>
        /// <param name="player">Hráč, který vstoupil na políčko</param>
        /// <param name="rolledValue">Hodnota hozená na kostce</param>
        public override void Enter(Player player, int rolledValue)
        {
            base.Enter(player, rolledValue);
            Debug.WriteLine("provede se úkol Finance TODO");
        }
    }
}
