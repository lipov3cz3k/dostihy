﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Racing
{
    internal class Distance : Field
    {
        /// <summary>
        /// Políčko distance
        /// </summary>
        /// <param name="location"></param>
        /// <param name="safeName"></param>
        public Distance(int location, string safeName)
            : base(location, safeName)
        {
            TypeOfField = FieldType.Distance;
        }

        /// <summary>
        /// Hráč vstoupil na políčko distance
        /// </summary>
        /// <param name="player">Hráč, který vstoupil na políčko</param>
        /// <param name="rolledValue">Hodnota hozená na kostce</param>
        public override void Enter(Player player, int rolledValue)
        {
            base.Enter(player, rolledValue);
            Debug.WriteLine("Hráč vstoupil do distance TODO");
            player.IsInDistance = true;
        }

    }
}
