﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Racing
{
    internal class Chance : Field
    {

        private CardStack cardStack;

        /// <summary>
        /// Karta náhoda
        /// </summary>
        /// <param name="location"></param>
        /// <param name="safeName"></param>
        public Chance(int location, string safeName, CardStack cardStack)
            : base(location, safeName)
        {
            TypeOfField = FieldType.Chance;
            this.cardStack = cardStack;
        }

        /// <summary>
        /// Hráč vstoupil na políčko, provede se úkol šance
        /// </summary>
        /// <param name="player">Hráč, který vstoupil na políčko</param>
        /// <param name="rolledValue">Hodnota hozená na kostce</param>
        public override void Enter(Player player, int rolledValue)
        {
            base.Enter(player, rolledValue);
            Debug.WriteLine("provede se úkol Šance TODO");
        }
    }
}
