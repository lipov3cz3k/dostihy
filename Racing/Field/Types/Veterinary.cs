﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Racing
{
    internal class Veterinary : Field
    {
        private int charge;

        /// <summary>
        /// Karta veterinární vyšetření
        /// </summary>
        /// <param name="location">Umístění na hrací desce</param>
        /// <param name="safeName">Bezpečné jméno (bez diakritiky a bílých znaků)</param>
        /// <param name="charge">Částka za veterinární vyšetří</param>
        public Veterinary(int location, string safeName, int charge)
            : base(location, safeName)
        {
            TypeOfField = FieldType.Veterinary;
            this.charge = charge;
        }
        
        /// <summary>
        /// Zaplatí poplatek za veterinární vyšetření
        /// </summary>
        public override void Paymant(Player player, int rolledValue)
        {
            player.PostPaymant(this.charge);
        }
    }
}
