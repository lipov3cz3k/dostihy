﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Racing
{
    /// <summary>
    /// Hrací políčko pro přepravu, nebo stáje
    /// </summary>
    internal class Utility : Buyable
    {
        private List<int> taxFactor;
        public List<Utility> otherUtilities;

        public Utility(int location, int price, List<int> taxFactor, string safeName)
            : base(location, price, safeName)
        {
            TypeOfField = FieldType.Utility;
            TaxFactor = taxFactor;
        }

        /// <summary>
        /// Zjistí možné akce s políčem (nákup, nákup dostihů, ...)
        /// </summary>
        /// <param name="Player"></param>
        /// <returns></returns>
        public override List<Action> AvailableActions(Player player)
        {
            List<Action> actions = new List<Action>();
            if (this.Owner == null && player != null)
            {
                // nikdo nevlastni
                if (player.Budget >= this.Price)
                    actions.Add(Action.CanBuy);
            }
            return actions;
        }

        public override void Paymant(Player player, int rolledValue)
        {
            if (this.Owner != null && this.Owner != player)
            {
                int amount = taxFactor[(this.otherUtilities.Where(utility => utility.Owner == this.Owner).Count())] * rolledValue;
                this.Owner.ReceivePaymant(amount);// hráč dostane zaplaceno vždy
                player.PostPaymant(amount);
            }
        }

        public List<int> TaxFactor
        {
            get { return taxFactor; }
            private set { taxFactor = value; }
        }
    }
}
