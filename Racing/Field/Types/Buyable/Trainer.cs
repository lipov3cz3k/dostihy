﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Racing
{
    /// <summary>
    /// Hrací políčko trenér
    /// </summary>
    internal class Trainer : Buyable
    {
        private int charge; // poplatek za vycvik
        public List<Trainer> otherTrainers; // pole s ostatnimi trenery

        /// <summary>
        /// Konstruktr trenéra
        /// </summary>        
        /// <param name="location">Číslo pozice na hrací desce</param>
        /// <param name="price">Nákupní cena trenéra</param>
        /// <param name="charge">Poplatek</param>
        public Trainer(int location, int price, int charge, string safeName)
            : base(location, price, safeName)
        {
            TypeOfField = FieldType.Trainer;
            Charge = charge;
        }

        /// <summary>
        /// Zjistí možné akce s políčem (nákup, nákup dostihů, ...)
        /// </summary>
        /// <param name="Player"></param>
        /// <returns></returns>
        public override List<Action> AvailableActions(Player player)
        {
            List<Action> actions = new List<Action>();
            if (this.Owner == null && player != null)
            {
                // nikdo nevlastni
                if (player.Budget >= this.Price)
                    actions.Add(Action.CanBuy);
            }

            return actions;
        }


        public override void Paymant(Player player, int rolledValue)
        {
            if (this.Owner != null && this.Owner != player)
            {
                int amount = this.Charge;
                amount += this.otherTrainers.Where(trainer => trainer.Owner == player).Sum(trainer => trainer.Charge);
                this.Owner.ReceivePaymant(amount);// hráč dostane zaplaceno vždy
                player.PostPaymant(amount);
            }
        }

        public int Charge
        {
            get { return charge; }
            set { charge = value; }
        }
    }
}
