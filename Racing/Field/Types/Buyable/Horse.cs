﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Racing
{
    /// <summary>
    /// Hrací políčko koně
    /// </summary>
    internal class Horse : Buyable
    {
        private List<int> charges; // poplatky
        private int tokenPrice; // cena žetonu
        private int nTokens; // počet žetonů
        private Stud stud; // do jaké stáje patří
        
        /// <summary>
        /// Konstruktor koně
        /// </summary>
        /// <param name="location">Číslo pozice na hrací desce</param>
        /// <param name="price">Nákupní cena koně</param>
        /// <param name="token">Cena jednoho žetonu</param>
        /// <param name="charges">Seznam poplatků</param>
        public Horse(int location, int price, int token, List<int> charges, string safeName)
            : base(location, price, safeName)
        {
            TypeOfField = FieldType.Horse;
            TokenPrice = token;
            Charges = charges;
        }


                /// <summary>
        /// Zjistí možné akce s políčem (nákup, nákup dostihů, ...)
        /// </summary>
        /// <param name="Player"></param>
        /// <returns></returns>
        public override List<Action> AvailableActions(Player player)
        {
            List<Action> actions = new List<Action>();
            if (this.Owner == null)
            {
                // nikdo nevlastni
                if(player.Budget >= this.Price)
                    actions.Add(Action.CanBuy);
            }
            if (this.Owner == player)
            {
                // hráč vlastní koně
                if (this.Stud.Horses.All(horse => horse.Owner == player))
                {
                    if (player.Budget >= this.tokenPrice && nTokens < 4)
                        actions.Add(Action.CanBuyOneToken);
                    else if (player.Budget >= 2 * this.tokenPrice && nTokens < 3)
                        actions.Add(Action.CanBuyTwoTokens);
                    else if (player.Budget >= 3 * this.tokenPrice && nTokens < 2)
                        actions.Add(Action.CanBuyThreeTokens);
                    else if (player.Budget >= 4 * this.tokenPrice && nTokens == 0)
                        actions.Add(Action.CanBuyFourTokens);
                    else if (player.Budget >= this.tokenPrice && nTokens == 4)
                        actions.Add(Action.CanBuyMainToken);
                }
            }
            return actions;
        }

        /// <summary>
        /// Zadaný hráč zaplatí prohlídku stáje vlastníkovy, pokud nějaký existuje
        /// </summary>
        /// <param name="player">Hráč, který platí (návštěvník)</param>
        public override void Paymant(Player player, int rolledValue)
        {
            if (this.Owner != null && this.Owner != player)
            {
                int amount = 0;
                // koně vlastní někdo cizí
                if (NTokens == 0 || !this.Owner.TokensIsActive())
                {
                    // platba za prohlídku koně
                    amount = this.Charges[0];
                }
                else
                {
                    // platba za dostihy
                    amount = this.Charges[NTokens];
                }
                this.Owner.ReceivePaymant(amount);// hráč dostane zaplaceno vždy
                player.PostPaymant(amount);
            }
            else
            {
                Debug.WriteLine("poplatek se neplatí");
            }
        }

        public List<int> Charges
        {
            get { return charges; }
            private set { charges = value; }
        }

        public int TokenPrice
        {
            get { return tokenPrice; }
            set { tokenPrice = value; }
        }

        public int NTokens
        {
            get { return nTokens; }
            set { nTokens = value; }
        }

        internal Stud Stud
        {
            get { return stud; }
            set { stud = value; }
        }

    }
}
