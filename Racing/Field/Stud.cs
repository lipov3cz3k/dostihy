﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Racing
{

    /// <summary>
    /// Třída stáje
    /// </summary>
    class Stud
    {

        private int id;
        private List<Horse> horses;



        /// <summary>
        /// Vytvoří novou stáj
        /// </summary>
        /// <param name="id">ID stáje</param>
        /// <param name="horses">Seznam koní které přiřazuju do stáje</param>
        public Stud(int id, ref List<Horse> horses)
        {
            Id = id;
            this.horses = horses;

            foreach (Horse horse in horses)
            {
                horse.Stud = this;
            }
        }

        /// <summary>
        /// Vrátí ID stáje
        /// </summary>
        public int Id
        {
            get { return id; }
            private set { id = value; }
        }

        public List<Horse> Horses
        {
            get { return horses; }
        }
    }
}
