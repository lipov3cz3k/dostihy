﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Racing;

namespace Racing
{
    public class Field
    {
        /// <summary>
        /// Typ herního políčka
        /// </summary>
        public enum FieldType
        {
            Safe = 0,
            Horse,
            Trainer,
            Utility,
            Chance,
            Finance,
            Veterinary,
            Distance,
            Doping
        };

        /// <summary>
        /// Akce, které jsou hráči k dispozici
        /// </summary>
        public enum Action
        {
            CanBuy,
            CanBuyOneToken,
            CanBuyTwoTokens,
            CanBuyThreeTokens,
            CanBuyFourTokens,
            CanBuyMainToken,
            CanSellTokens
        }

        private int location;
        private FieldType typeOfField;
        private string safeName;


        /// <summary>
        /// Konstruktor políčka
        /// </summary>
        /// <param name="location">Pozice herního políčka</param>
        /// <param name="safeName">Jméno podle kterého se vyhledává</param>
        public Field(int location, string safeName)
        {
           TypeOfField = FieldType.Safe;
           Location = location;
           SafeName = safeName;
        }

        public virtual void Paymant(Player player, int rolledValue)
        {

        }

        /// <summary>
        /// Player entered this field, he make payment for it.
        /// </summary>
        /// <param name="player">Current player</param>
        /// <param name="rolledValue">Dice value</param>
        public virtual void Enter(Player player, int rolledValue)
        {
            Debug.WriteLine("Hráč " + player.Name + " hodil hodnotu " + rolledValue + " posunul se na " + "[" + this.Location + "]" + this.SafeName);
            Paymant(player, rolledValue);

        }

        /// <summary>
        /// Check all posible actions over field (buy field, buy tokens, ...)
        /// </summary>
        /// <param name="Player">Current player</param>
        /// <returns>List of possible actions</returns>
        public virtual List<Action> AvailableActions(Player player)
        {
            return new List<Action>();
        }

        /// <summary>
        /// Vrátí jméno políčka v bezpečném tvaru (bez mezer a diakritiky)
        /// </summary>
        public string SafeName
        {
            get { return safeName; }
            set { safeName = value; }
        }

        /// <summary>
        /// Pozice políčka na herní desce
        /// </summary>
        public int Location
        {
            get { return location; }
            private set { location = value; }
        }

        /// <summary>
        /// Typ políčka
        /// </summary>
        public FieldType TypeOfField
        {
            get { return typeOfField; }
            protected set { typeOfField = value; }
        }
    }
}
