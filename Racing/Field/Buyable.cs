﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Racing
{
    /// <summary>
    /// Třída majetku - políčka která se dají koupit
    /// </summary>
    internal class Buyable : Field
    {
        /// <summary>
        /// Price of field
        /// </summary>
        private int price;

        /// <summary>
        /// Owner of field
        /// </summary>
        private Player owner;

        /// <summary>
        /// Buyable field
        /// </summary>
        /// <param name="location">Location on the board</param>
        /// <param name="price">Price of the field</param>
        /// <param name="safeName">Safe name of the field (without diacritic and white chars)</param>
        public Buyable(int location, int price, string safeName)
            : base(location, safeName)
        {
            Price = price;
        }

        /// <summary>
        /// Assign new owner and reqest paymant for this.
        /// </summary>
        /// <param name="player">New owner</param>
        public void Buy(Player player)
        {
            this.Owner = player;
            player.PostPaymant(this.Price);
        }

        /// <summary>
        /// Return true, of field has owner
        /// </summary>
        /// <returns>True, of field has owner</returns>
        public bool IsOwned()
        {
            if (this.owner != null)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Gets or sets field's owner
        /// </summary>
        public Player Owner
        {
            get { return owner; }
            set { owner = value; }
        }


        /// <summary>
        /// Gets or sets field's price
        /// </summary>
        public int Price
        {
            get { return price; }
            private set { price = value; }
        }
    }
}
